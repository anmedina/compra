def main(especifica = None)
    habitual = ["agua", "huevos", "pan" ]
    especifica = []

    elemento = input("Elemento a comprar: ")
    while elemento != "":
        if elemento not in especifica:
            especifica.append(elemento)
        elemento = input("Elemento a comprar: ")

    lista_final = habitual.copy()
    for k in especifica:
        if k not in habitual:
            lista_final += [k]

    print("Lista de la compra: ")
    for n in lista_final:
        print(n)

    print("Elementos habituales: ", len(habitual))
    print("Elementos especificos: ", len(especifica))
    print("Elementos en lista: ", len(lista_final))
    if __name__ == '__main__':
        main()
